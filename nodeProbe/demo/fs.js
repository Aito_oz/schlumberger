const fs = require('fs') //fs - file system
const path = require('path')
const text = require('../data')

// fs.mkdir(path.join(__dirname, 'test'), (err) => { //создает файл, и не может создать еще один с таким же именем
//     if (err) {
//         throw err
//     }
//     console.log('Папка создана');
// })

const filePath = path.join(__dirname, 'test', 'text.txt')

// fs.writeFile(filePath, 'Hello NodeJS!...', err => { // может перетереть уже существующий файл
//     if (err) {
//         throw err
//     }
//     console.log('Файл создан')
// })

// fs.appendFile(filePath, '\nHelloucha', err => { // принимает те же параметры, но фукнционал немного отличается, он добавляет к файлу данные
//     if (err) {
//         throw err
//     }
//     console.log('Файл обновлен')
// })

// Чтение файлов

fs.readFile(filePath, (err, content) => {
    if (err) {
        throw err
    }
    const data = Buffer.from(content) // передаем то значет, которые получили из буфера
    // console.log('Content: ', content) // по умолчанию nodejs возращает буферы, так он все считывает в буферах по умолчанию
    console.log('Content: ', data.toString()) // переводим это значение в строку
})