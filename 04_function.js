// Функции

// Function Declaration. Мы можем обращаться к функции когда захотим. То есть можно вызвать ее до того, как создадим
// function greet(name) {
//     console.log(`Привет ${name}`)
// }

//Function Expression. Здесь ситуация наоборот. Мы не сможем вызвать функцию до того как создадим ее.
// при передачи функции в переменную нам не обязательно ее называть, но можно так же написать funtion great2()
// const greet2 = function(name) {
//     console.log(`Привет2 ${name}`)
// }
// greet('R')
// greet2('r')

// console.log(typeof greet) //выведет function, хотя по сути в js Нет такого типа данных как function

// console.dir(greet) //что такое функция на самом деле. то есть это объект

// Анонимные функции
// let counter = 0
// setInterval(function() {
//     console.log(++counter)
// }, 1000) //каждые 1000 милисекунд каунтер увеличивается на 1. Но речь о том, что в функцию setInterval мы передаем анонимную функцию

// let counter = 0
// const interval = setInterval(function() {
//     if (counter === 5) {
//         clearInterval(interval)
//     } else {
//         console.log(++counter)
//     }
// }, 1000)

// Стрелочные функции

// function greet(name) {
    // console.log(`Привет ${name}`)
// }

// const arrow = (name) => {
    // console.log(`Привет ${name}`)
// }                                   // сама стрелочная функция определяется благодаря =>

// const arrow2 = name => console.log(`Привет ${name}`) // так как параметр один, то можно указать просто name, и так же раз строка в функции одна, то можно просто после стрелки указать допустим console.log

// const pow2 = num => num ** 2 //можно не писать return для вывода если строка всего одна

// console.log(pow2(5))

// Параметры по умолчанию
// const sum = (a, b) => a + b
// console.log(sum(41, 2)) //все будет окей, выдаст 43
// console.log(sum(41)) //получим NaN, чтоб этого избежать, то можно передать параметр по умолчанию

// const sum2 = (a, b = 2) => a + b
// console.log(sum2(41)) // и снова получаем 43. Если же мы впишем свой параметр, то он перезапишет параметр по умолчанию

// специальный оператор rest , который обозначается как ..., собирает все числа в массив
// function sumAll(...all) {
//     console.log(all)
//     let result = 0
//     for (let num of all) {
//         result += num
//     }
//     return result
// }

// sumAll(1, 2, 3, 4, 5)
// а теперь, благодаря циклу for посчитаем сумму чисел в all
// const res = sumAll(1, 2, 3, 4, 5)
// console.log(res)

// Замыкания. Т.е. когда из одной функции возвращаем другую
// function createMember(name) {
//     return function(lastName) {
//         console.log(name + lastName)
//     }
// }

// const logWithLastName = createMember ('Aito')
// console.log(logWithLastName) // выдаст только конструкцию функции
// console.log(logWithLastName(' Oz')) //а здесь name сохраняется которое мы указали ранее, а вот lastName мы указываем в этом вызове