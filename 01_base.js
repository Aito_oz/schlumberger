// 1. Переменные /////////////////////////////////////////
// camelCase стаилгайд которого стоит придерживаться
// const nickName = 'oz'
// const region = 98
// const TrueFalse = true

// const _private = 'private'
// const $ = 'dollar'

// const withNum5 = 'qwerty'
// const 5withNum = 5 //err
// const if = "sdf" // err


// 2. Мутирование

// console.log('Ник: ' + nickName + ', регион СПб: ' + region)
// alert('Ник: ' + nickName + ', регион СПб: ' + region) // доступна только в браузере

// prompt('Введите фамилию')
// с помощью prompt можно получать данные от пользователя, например:
// const city = prompt('Введите город')
// alert(nickName + ' из ' + city)

// 3. Операторы /////////////////////////////////////////

// let currentYear = 2022
// const sonBirthday = 2012
// const ageSon = currentYear - sonBirthday

// const a = 20
// const b = 11
// let c = 32

// c = c + a
// c += a // как и в питоне можно сокращать

// console.log(a + b)
// console.log(a - b)
// console.log(a * b)
// console.log(a / b)

// console.log(currentYear++) // Оператор ++ стоящий после числа сначала возвращает число до изменения и перезаписывает его прибавляя 1, если поставить ++ перед числом, то вернется сразу число+1 (для оператора -- аналогичная ситуация)
// console.log(currentYear)
// console.log(c)

// 4. Типы данных (примитивы) /////////////////////////////////////////

// const anyBoo = true
// const stroka = 'string'
// const integer = 28
// let x // в переменные let нам необязательно указывать значения, а вот в const обязательно надо их указывать.

// console.log(typeof anyBoo)
// console.log(typeof stroka)
// console.log(typeof integer)
// console.log(typeof x)
// console.log(typeof null) //в данном случае typeof не всегда возвращает нам конкретный тип данных (сейчас он вернул значение object)


// 5. Приоритет операторов 

// документация по JS - mdn (mozilla development network)

// const fullAge = 27
// const birthYear = 1994
// const currentYear = 2022

// const isFullAge =  currentYear - birthYear >= fullAge

// console.log(isFullAge)

// 6. Условные операторы

const courseStarus = 'pending' // здесь могут быть различные статусы: ready, fail, pending

// if (courseStarus === 'ready') {
//     console.log('Какая-то информация которая выводится если if выполняется')
// } else if (courseStarus === 'pending') {
//     console.log('А это другая информация которая выводится если if выполняется')
// } else {
//     console.log('А тут описываем что условия невыполнены')
// }

const isReady = false

// если это булево значение то мы можем записать как if (isReady === true){} либо if(isReady){}

// if (isReady) {
//     console.log('короткая запись')
// } else {
//     console.log('что-то пошло не так')
// }
// можно еще больше сократить запись if и после : идет блок else

// isReady ? console.log('ооооочень короткая запись') : console.log('ну или нет') // тернарное выражение


        // что означают == и ===
// const num1 = 42 // number
// const num2 ='42' // string

// console.log(num1 == num2) // == приводит все к одному типу данных. в этом случае вернется true
// console.log(num1 === num2) // === сравнивает и типы данных. в этом случае вернется false


// 7. булевая логика
// && и,  || или, ! "не" можно применять несколько раз. Всё это можно прочитать в документации мозилы

// 8. Функции

// function calculateAge(year) {
//     return 2022 - year
// }

// const myAge = calculateAge(1994)

// console.log(myAge)

// function logInfoAbout(name, year) {
//     const age = calculateAge(year)

//     console.log('Возраст '+ name + ' составляет ' + age + ' лет.')
// }

// logInfoAbout('Aito', 1994)

// помимо функции в функции можем в функцию класть и условные операторы

// function logInfoAbout(name, year) {
//     const age = calculateAge(year)

//     if (age > 0) {
//         console.log('Возраст '+ name + ' составляет ' + age + ' лет.')
//     } else {
//         console.log('а человек то еще не родился...')
//     }
// }

// logInfoAbout('Aito', 1994)

// 9. Массивы

// const city = ['LA', 'NY', 'Msc', 'SPb'] 

// console.log(city)  

// const city = new Array('LA', 'NY', 'Msc', 'SPb') // создавать массивы в js можно и так, но лучше делать как в первом варианте

// console.log(city)
// console.log(city[1]) // можем возвращать опеределенные данные из масива по индексу
// console.log(city.length) // узнать длинну массива
// несмотря на то, что массив определен через const, мы можем менять его

// city[1] = 'Tyumen'
// console.log(city) 
// city[4] = 'Los-Angeles' // можно расширять массив через статический индекс
// console.log(city) 

// city[city.length] = 'Surgut' //можно расширять массив через динамический индекс. Например добавить в конец массива
// console.log(city) 

// 10. Циклы
// const city = new Array('LA', 'NY', 'Msc', 'SPb')

// for (let i = 0; i < city.length; i++) { // можем через цикл показать с шагом 1 количество элементов в массиве
    // console.log(i)
// }

// for (let i = 0; i < city.length; i++) { // а можем показать каждый элемент по порядку
    // const cities = city[i]
    // console.log(cities)
// }

// но в последних версиях js можно сделать удобнее. В цикле создать переменну которая обозначает элемент массива

// for (let cities of city) {
    // console.log(cities)
// }

// 11. Объекты

// лучш создавать объект через фигурные скобки {}
// const person = {
    // nickName: 'Aito', // в объект можно записывать строки
    // region: 98, // числа
    // year: 1994,
    // languages: ['Ru', 'En', 'Fr'], //массивы
    // hasWife: true, //булевые значения
    // greet: function() { //так же можно создавать функции. Но так как она в объекте, то это метод
        // console.log('супер')
    // }
// }

// как же пользоваться объектами

// console.log(person) // просто вызвать объект
// console.log(person.nickName) // можно получить конкретное значение
// console.log(person['region']) // конкретное значение через квардратные скобки []
// person.greet() //это для примера вызов функции

// const key = 'hasWife'
// console.log(person[key]) //вызвать значение через переменную

// person.region = 72 // можем менять ключи
// console.log(person.region)



