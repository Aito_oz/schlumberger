// 1. Строки
// const stroka = 'строка'
// const num = 13

// function getNum() {
//     return num
// }

// const output = `Ляля ${stroka} и число ${getNum()}` //благодаря обратным ковычкам можно вставлять переменные или функции в строку с помощью ${}. Нельзя ставить условыне операторы типа if, но можем использовать тернарные выражения ${num < 30 ? 'A' : 'B'}

// console.log(output)

// const output = `   
//     <div>какой-то текст</div>
//     <p>какой-то параграф</p>
// `
// console.log(output)          //не нужны запятые, строка и сама запоминает переносы

// const stroka = 'Строка' // основные методы строк
// console.log(stroka.length) // сколько символов в строке
// console.log(stroka.toUpperCase()) // перевести в верхный регистр
// console.log(stroka.toLowerCase()) // перевести в нижний регистр
// console.log(stroka.charAt(2)) // узнать символ по индексу
// console.log(stroka.indexOf('рок')) // узнать индекс элемента в строке, верне с какого индекса он начинается
// console.log(stroka.indexOf('!')) // если элемент не присутствует, то получим значение -1 
// console.log(stroka.startsWith('ст')) // Начинается ли строка с такого-то значения. В данном случае получим false
// console.log(stroka.toLowerCase().startsWith('стр')) // приводим в нижний регистр и проверяем. получаем true
// console.log(stroka.endsWith('ка')) // Оканчивается строка таким значением
// console.log(stroka.repeat(3)) //продублировать строку определенное количество раз

// const stroka2 = '   password   ' // пробел учитывается в строке и так же будет отображаться, но в случае с паролем это может оказаться багом
// console.log(stroka2.trim()) //trim() Очищает все пробелы, либо trimLeft(), trimRight(), чтоб убрать пробелы слева или справа

// function logPerson(s, name, age) {
//     if (age < 0) {
//         age = 'bla bla bla'
//     }
    // console.log(s, name, age)
//     return `${s[0]}${name}${s[1]}${age}${s[2]}`
// }

// const personName = 'Aito'
// const personName2 = 'Oz'
// const personAge = 28
// const personAge2 = -10

// const output = logPerson`Имя: ${personName}, Возраст: ${personAge}!`
// const output2 = logPerson`Имя: ${personName2}, Возраст: ${personAge2}!`

// console.log(output)
// console.log(output2)
