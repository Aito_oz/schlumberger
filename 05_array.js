// Массивы
const cities = ['LA', 'NY', 'Tyumen', 'SPb', 'Msc'] // можно хранить строки
// const people = [
//     {name: 'Aito', region: 72},
//     {name: 'Eugene', region: 86},
//     {name: 'Gesha', region: 98},
// ]
const fib = [1, 1 , 2, 3, 5, 8, 13] //числа. Можно и совмещать, а так же и булевы значения и объекты

// Method
// cities.push('Surgut') //более привычный и современный метод для добавления объекта в массив (в конец)
// cities.unshift('Brooklyn') // Добавляет элемент в начало массива
// cities.shift() // вернет первый элемент и удалит его из массива
// cities.pop() //возвращает последний элемент и удаляет его из массива
// cities.reverse() //переворачивает массив

// задач 1
// const text = 'я добьюсь своей цели'
// const reverseText = text.split() // делит строку на элементы и создает массив. В скобках указываем сам разделитель, например запятую или пробел (' '), (','), или просто разделить на каждый символ то ('')
// const reverseText = text.split('').reverse()
// console.log(reverseText)
// const joinText = reverseText.join('') // работает как и split но уже соеденяет элементы массива и преобразует в строку
// console.log(joinText)

// console.log(cities.indexOf('Tyumen')) // получаем индекс элемента массива. Подходит для простых элементов. Строки, числа например

// const index = people.find(function(person) {
//     return person.region === 98
// })  
// const index = people.findIndex (function(person) {
//     return person.region === 98
// })                      //этот метод подходит для более сложных массивов

// let findedPerson            // можно найти конечно и через цикл, но верхний способ проще
// for (const person of people) {
    // console.log(person)
//     if (person.region === 98) {
//         findedPerson = person
//     }
// }

// еще более локаничный способ
// const index = people.find(function(person) {
//     return person.region === 98
// })  

// const person = people.find(person => person.region === 72) // лямбда функция

// cities.includes('city') // узнаем есть ли такой элемент в массиве и получаем в ответ true or false

// const upperCaseCities = cities.map(city => {
//     return city.toUpperCase()
// })
// console.log(upperCaseCities) // переводим все элементы массива в верхний регистр. Но map() вернет уже новый массив, а старый останется без изменений

// const pow2 = num => num ** 2
// const sqrt = num => Math.sqrt(num)

// const pow2Fib = fib.map(pow2).map(sqrt) //так как map() принимает функции, мы можем задать переменную с функции и передать ее в map() а не вызывать в нем

// const pow2 = num => num ** 2
// const pow2Fib = fib.map(pow2) // map()преобразовывает данные

// const filteredNumbers = pow2Fib.filter(num => num > 20) // filter() фильтрует данные
// console.log(pow2Fib)
// console.log(filteredNumbers) //можем фильтровать массивы // reduce() первым параметром принимает функцию, а потом значение, начиная с которого мы ходим считать. Функция же принимает в себя некий аккамулятор (acc), то значение которое будет переоформляться на каждой итерации массива, а потом тот элемент на который мы делаем итерацию. Здесь мы все так же можем использовать стрелочные функции. reduce() объеденяет все значения массива в одно значение

const people = [
    {name: 'Aito', budget: 4200},
    {name: 'Eugene', budget: 3700},
    {name: 'Gesha', budget: 1200}
]

const allBudget = people
    .filter(person => person.budget > 2000)
    .reduce((acc, person) => {
        acc += person.budget
        return acc
    }, 0)

console.log(allBudget)