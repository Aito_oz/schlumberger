// 1. Number -----------------------------------------------------
// const num = 13 // можем создавать переменные
// const num1 = 14 // integer целое число
// const num2 = 14.2 // float число с плавоющей точкой
// const pow = 10e3 // числа в степени. тут получается 10000

// console.log('MAX_SAFE_INTEGER', Number.MAX_SAFE_INTEGER) // максимальное целое число в js  9007199254740991. Это 2 в 53 степени - 1 (для выполнения операций)

// как сделать найти число самостоятельно
// console.log('Math.pow', Math.pow(2, 53) - 1)

// можно получить и более высокие значения
// console.log('MIN_SAFE_INTEGER', Number.MIN_SAFE_INTEGER) // минимальное целое число

// console.log('MAX_VALUE', Number.MAX_VALUE) // максимальное значение которым может оперировать js
// console.log('MIN_VALUE', Number.MIN_VALUE) // минимальное значение которым может оперировать js

// console.log('POSITIVE_INFINITY', Number.POSITIVE_INFINITY) // позитивная бесконечность
// console.log('NEGATIVE_INFINITY', Number.NEGATIVE_INFINITY) // негативная бесконечность

//чтобы получить бесконечность, мы можем любое число разделить на 0
// console.log('1 / 0', 1 / 0)

// console.log(Number.NaN) //Not A Number, то есть специальное значение которое не является числом

//но если посмотреть на тип NaN, то мы получим тип number
// console.log(typeof NaN)
//мы можем его получить если разделим число на undefined
// console.log(2 / undefined)

// const weird = 2 / undefined
// console.log(isNaN(weird)) //функция isNaN проверяет является ли переменная NaN и выведет true
// console.log(isNaN(13))  // если мы проверим обычное число то, естественно, получим false

// console.log(Number.isFinite(Infinity)) //Проверяем конечное ли число, получаем false писать Number не обязательно
// console.log(isFinite(40)) // Проверяем конечное ли число, получаем true

// const stringInt = '42'
// const stringFloat = '42.42'

// console.log(stringInt + 2) // мы получим не 44, а 422, так как 42 это строка, а у строки нет операции сложения, то js считает что это все будет строка и мы получаем 422

// console.log(parseInt(stringInt) + 2) // можно привести строку к целому числу с помощью метода parseInt (parsing intenger) и тогда выведется уже число 44

// console.log(Number(stringInt) + 2) // можно просто переменную обернуть в NUmber

// console.log(+stringInt + 2) // а еще короче можно поставить + перед переменной и js преобразует строку в число

// console.log(parseInt(stringInt)) // метод сделает из числа с точкой целое число
// console.log(parseFloat(stringFloat)) // для чисел с точкой есть свой метод, который приобразует строку в число с плавоющей точкой
// console.log(+stringFloat) // либо также можем поставить +

// console.log(0.4 + 0.2) // ожидаем, конечно же, 0.6, но получим 0.6000000000000001. Как решить это?

// console.log((0.4 + 0.2).toFixed(4)) //вызываем метод toFixed для числа. и в () указываем количество знаков после точки. Но это будет строка, а не число

// console.log(parseFloat((0.4 + 0.2).toFixed(1))) // Оба метода сделают из строки число
// console.log(+((0.4 + 0.2).toFixed(4)))          // и лишние нули этот метод уберет


// 2. BigInt ( еще один тип данных) -----------------------------------------------------
// его ввели чтобы мы могли работать с данным которые больше чем MAX_SAFE_INTEGER

// console.log(typeof 9007199254740991999999999) // это number
// console.log(typeof 9007199254740991999999999n) // а это bigint
// проводить операции можно только если все элементы это bigint
// console.log(typeof 9007199254740991999999999.2323n) // error

// console.log(10n - 4) //будет ошибка, так как разные типы данных

// console.log(BigInt(4)) //Обычное число преобразуется в bigint

// console.log(5n / 2n) //получим 2n так как это bigint ( то есть цело число )

// 3. Math --------------------------------------------------------------------------------
// console.log(Math.E)     // E - экспонента. Это константа
// console.log(Math.PI)    // PI - число Пи. Тоже константа

// console.log(Math.sqrt(16)) // квадратный корень из числа
// console.log(Math.pow(2, 3)) // возвести 2 в степень 3
// console.log(Math.abs(-42)) // модуль числа. Отсекаем негативную часть и оставляем только модуль, то есть, в данном случае, 42
// console.log(Math.max(42, 12, 124)) // определяет максимальное число из списка
// console.log(Math.min(42, 12, 124)) // определяет минимальное число из списка
// console.log(Math.floor(4.6)) // Округляет значение. Всегда в меньшую сторону
// console.log(Math.ceil(4.1)) // округляет значение в большую сторону
// console.log(Math.round(4.6)) // округляет к ближайшему числу
// console.log(Math.trunc(4.9)) // не округляет, а просто удаляет знаки после запятой
// console.log(Math.random()) // не принимает никаких параметров но выдает рандомное значение числа 

// 4. Example (пример)
// создаем функцию, которая будет возвращать случайное число в диапазоне 2х целых чисел

// function randNum(min, max) {
//     return Math.random() * (max - min) + min // хз что за алгоритм такой, как он получается, я не понял
// }

// console.log(randNum(14, 72))

// function randNum(min, max) {
//     return Math.floor(Math.random() * (max - min + 1) + min) // чтобы округлить мы зачем то помимо нового метода прибавили 1 к алгоритму
// }

// console.log(randNum(14, 72))